/* eslint-disable*/
fetch('http://localhost:5000/companyData.json')
  .then(resp => resp.json())
  .then((data) => {
    Highcharts.chart('container1', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'AUTHORIZED CAPITAL IN INTERVALS',
      },
      xAxis: {
        categories: ['capitalLessThan1L', 'capital1LTo10L', 'capital10LTo1Cr', 'capital10CrTo100Cr', 'capitalMoreThan100Cr'],
      },
      yAxis: {
        title: {
          text: 'AUTHORIZED CAP',
        },
      },
      series: [{
        name: 'AUTHORIZED CAPITAL',
        data,
      }],
    });
  });
