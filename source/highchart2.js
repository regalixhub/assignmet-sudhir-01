/* eslint-disable*/
fetch('http://localhost:5000/dateOfRegdData.json')
  .then(resp => resp.json())
  .then((data) => {
    const year = Object.keys(data);
    const numofreg = Object.values(data);
    Highcharts.chart('container2', {
      chart: {
        type: 'pie',
      },
      title: {
        text: 'Number of Registrations in a year',
      },
      xAxis: {
        categories: year,
      },
      yAxis: {
        title: {
          text: 'Year',
        },
      },
      series: [{
        name: 'Number of Company Registrations',
        data: numofreg,
      }],
    });
  });