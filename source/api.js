
const fs = require('fs');
const CsvReadableStream = require('csv-reader');

const csvFilePath = './officedata.csv';
// Subtask1
async function calculateCapital() {
  return new Promise((resolve) => {
    let c1 = 0;
    let c2 = 0;
    let c3 = 0;
    let c4 = 0;
    let c5 = 0;
    const inputStream = fs.createReadStream(csvFilePath, 'utf-8');
    inputStream
      .pipe(CsvReadableStream({
        parseNumbers: true, parseBooleans: true, trim: true, skipHeader: true,
      }))
      .on('data', (capital) => {
        if (capital[8] < 1e5) {
          c1 += 1;
        } else if (capital[8] >= 1e5 && capital[8] < 1e6) {
          c2 += 1;
        } else if (capital[8] >= 1e6 && capital[8] < 1e7) {
          c3 += 1;
        } else if (capital[8] >= 1e8 && capital[8] < 1e9) {
          c4 += 1;
        } else if (capital[8] >= 1e9) {
          c5 += 1;
        }
      })
      .on('end', () => {
        const result = [c1, c2, c3, c4, c5];
        resolve(result);
      });
  });
}
async function calculatedCapital() {
  const AuthorisedCapital = await calculateCapital();
  return AuthorisedCapital;
}
async function numberOfRegistrationPerYear() {
  return new Promise((resolve) => {
    let date;
    const dor = {};
    const inputStream = fs.createReadStream(csvFilePath, 'utf-8');
    inputStream
      .pipe(CsvReadableStream({
        parseNumbers: true, parseBooleans: true, trim: true, skipHeader: true,
      }))
      .on('data', (data) => {
        date = data[6].split('-');
        if (data[6] !== 'DATE_OF_REGISTRATION') {
          if (!dor[date[2]]) {
            dor[date[2]] = 1;
          } else {
            dor[date[2]] += 1;
          }
        }
      })
      .on('end', () => {
        resolve(dor);
      });
  });
}
// SUBTASK NUMBER 3
async function PBActivityof2015() {
  return new Promise((resolve) => {
    const pba = {};
    let date;
    const inputStream = fs.createReadStream(csvFilePath, 'utf-8');
    inputStream
      .pipe(CsvReadableStream({
        parseNumbers: true, parseBooleans: true, trim: true, skipHeader: true,
      }))
      .on('data', (data) => {
        date = data[6].split('-');
        if (date[2] === '2015') {
          if (pba[data[11]] === undefined) {
            pba[data[11]] = 1;
          } else {
            pba[data[11]] += 1;
          }
        }
      })
      .on('end', () => {
        resolve(pba);
      });
  });
}
async function registrationCountPerYear() {
  return new Promise((resolve) => {
    let date;
    const aggr = {};
    let year;
    const inputStream = fs.createReadStream(csvFilePath, 'utf-8');
    inputStream
      .pipe(CsvReadableStream({
        parseNumbers: true, parseBooleans: true, trim: true, skipHeader: true,
      }))
      .on('data', (data) => {
        date = data[6].split('-');
        if (date[2] >= '2010' && date[2] <= '2018') {
          if (!aggr[data[11]]) {
            aggr[data[11]] = {};
            year = aggr[data[11]];
            if (!year[date[2]]) {
              year[date[2]] = 1;
            }
          } else {
            year = aggr[data[11]];
            if (!year[date[2]]) {
              year[date[2]] = 1;
            } else {
              year[date[2]] += 1;
            }
          }
        }
      })
      .on('end', () => {
        resolve(aggr);
      });
  });
}
module.exports = {
  calculatedCapital,
  numberOfRegistrationPerYear,
  PBActivityof2015,
  registrationCountPerYear,
};
