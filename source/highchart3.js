/* eslint-disable*/
fetch('http://localhost:5000/PBAData.json')
  .then(resp => resp.json())
  .then((data) => {
    const keysSorted = Object.keys(data).sort((a, b) => data[b] - data[a]);
    const pba = [];
    const numofreg = [];
    for (let i = 0; i < 10; i += 1) {
      pba.push(keysSorted[i]);
      numofreg.push(data[keysSorted[i]]);
    }
    Highcharts.chart('container3', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Top registrations by "Principal Business Activity" for the year 2015',
      },
      xAxis: {
        categories: pba,
      },
      yAxis: {
        title: {
          text: 'Year',
        },
      },
      series: [{
        name: 'Number of Company Registrations',
        data: numofreg,
      }],
    });
  });