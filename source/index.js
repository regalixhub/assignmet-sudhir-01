const fs = require('fs');
const api = require('./api.js');

api.calculatedCapital().then((res) => {
  fs.writeFile('companyData.json', JSON.stringify(res));
});
api.numberOfRegistrationPerYear().then((res) => {
  fs.writeFile('dateOfRegdData.json', JSON.stringify(res));
});
api.PBActivityof2015().then((res) => {
  fs.writeFile('PBAData.json', JSON.stringify(res));
});
api.registrationCountPerYear().then((res) => {
  fs.writeFile('aggrData.json', JSON.stringify(res));
});
