/* eslint-disable*/
fetch('./aggrData.json')
  .then(resp => resp.json())
  .then((data) => {
    const year = [2010];
    for (let i = 0; year[i] < 2019; i += 1) {
      year.push(1 + year[i]);
    }
    const serarr = [];
    const pba = Object.keys(data);
    for (let i = 0; i < pba.length; i += 1) {
      serarr.push(
        {
          name: pba[i],
          data: Object.values(data[pba[i]]),
        },
      );
    }
    Highcharts.chart('container4', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Stacked-bar-chart',
      },
      xAxis: {
        categories: year,
      },
      yAxis: {
        title: {
          text: 'Year',
        },
      },
      plotOptions: {
        series: {
          stacking: 'normal',
        },
      },
      series: serarr,
    });
  });